FROM nginx:stable-alpine

WORKDIR /usr/share/nginx/html/hello-word/app

COPY /docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY . /usr/share/nginx/html/hello-word

EXPOSE 8080
